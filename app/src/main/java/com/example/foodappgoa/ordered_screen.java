package com.example.foodappgoa;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ordered_screen extends AppCompatActivity {

    TextView listView, priceView;
    String list_choices;
    Double price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordered_screen);
        listView = (TextView) findViewById(R.id.listView);
        priceView = (TextView) findViewById(R.id.priceView);

        Bundle bundle = getIntent().getExtras();
        list_choices = bundle.getString("choices");
        price = bundle.getDouble("price");

        listView.setText(list_choices);
        priceView.setText(price.toString());
    }
}