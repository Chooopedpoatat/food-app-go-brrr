package com.example.foodappgoa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class menu_screen extends AppCompatActivity {

    Button burgerButton;
    String choices = "";
    Double price = 0.00;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_screen);

        burgerButton = findViewById(R.id.burgerButton);
    }

    public void add_to_list(View view) {
        if (view == findViewById(R.id.burgerButton)){
            choices = choices+"burger"+"\n";
            price = price + 1000;
        }
    }

    public void placeOrder (View view){
        Intent i = new Intent(menu_screen.this, ordered_screen.class);
        Bundle bundle = new Bundle();
        bundle.putString("choices", choices);
        bundle.putDouble("price", price);
        i.putExtras(bundle);
        startActivity(i);
    }

}

